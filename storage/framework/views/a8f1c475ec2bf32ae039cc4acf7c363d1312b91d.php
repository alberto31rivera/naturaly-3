<?php $__env->startSection('content'); ?>
<link href="<?php echo e(dsAsset('site/css/custom/choose-payment-method.css')); ?>" rel="stylesheet" />
<div class="row">
    <div class="col-md-4 offset-md-4 mt-4">
        <div class="main-card  card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">
                        <?php echo e(translate('Choose your desired payment partner')); ?>

                    </h4>

                </div>
            </div>
            <div class="card-body">
                <div class="row d-flex justify-content-center">
                    <div class="w100" id="divPaymentMethod">
                        <?php $__currentLoopData = $paymentMethod->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pay): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($pay['type']!=1): ?>

                        <div class="payment-chose-div float-start <?php echo e($pay['type']==2?'payment-chose':''); ?>">
                            <input <?php echo e($pay['type']==2?'checked':''); ?> type="radio" name="payment_type" value="<?php echo e($pay['id']); ?>" class="float-start payment-radio d-none" />
                            <div class="float-start color-black p-2">
                                <?php if($pay['type']==2): ?>
                                <img src="img/payment-paypal.svg" class="img-fluid w-100" />
                                <?php elseif($pay['type']==3): ?>
                                <img src="img/payment-stripe.svg" class="img-fluid" />
                                <?php elseif($pay['type']==4): ?>
                                <img src="img/payment-user-balance.svg" class="img-fluid" />
                                <?php elseif($pay['type']==5): ?>
                                <img src="img/yape.jpg" class="img-fluid" />
                                <?php elseif($pay['type']==6): ?>
                                <img src="img/plin.jpg" class="img-fluid" />
                                <?php elseif($pay['type']==7): ?>
                                <img src="img/transferencia.jpg" class="img-fluid" />
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col mt-5 d-flex justify-content-center">
                        <button id="btnNext" type="button" class="btn btn-booking btn-lg"><?php echo e(translate('Process To Pay')); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(dsAsset('site/js/custom/choose-payment-method.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.site', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/acampos/Workspace/naturaly-3/resources/views/site/choose-payment-method.blade.php ENDPATH**/ ?>