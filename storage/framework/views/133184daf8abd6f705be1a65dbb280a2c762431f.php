<!DOCTYPE html>
<html lang="es" class="no-js" dir="<?php echo e($rtl); ?>">
<head>
	<meta name="_token" content="<?php echo e(csrf_token()); ?>" url="<?php echo e(url('/')); ?>" />
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?php echo e(url($appearance->icon)); ?>">
	<!-- Meta Description -->
	<meta name="description" content="<?php echo e($appearance->meta_description); ?>">
	<!-- Meta Keyword -->
	<meta name="keywords" content="<?php echo e($appearance->meta_keywords); ?>">
	<!-- meta character set -->
	 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Site Title -->
	<title><?php echo e($appearance->app_name); ?></title>
	 
	<link rel="stylesheet" href="<?php echo e(dsAsset('css/app.css')); ?>">

	<script src="<?php echo e(dsAsset('site/assets/js/jquery.min.js')); ?>"></script>


	<link rel="stylesheet" href="<?php echo e(dsAsset('site/assets/js/lib/icofont/icofont.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(dsAsset('site/assets/js/lib/fontawesome/css/all.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(dsAsset('site/assets/js/lib/owl-carousel/assets/owl.theme.default.min.css')); ?>">
 
<!-- 	<link rel="stylesheet" href="<?php echo e(dsAsset('site/assets/css/app.css')); ?>"> -->
	<link href="<?php echo e(dsAsset('js/lib/xd-dpicker/jquery.datetimepicker.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(dsAsset('js/lib/tel-input/css/intlTelInput.css')); ?>" rel="stylesheet" />

	<!-- datetime pciker js -->
	<script src="<?php echo e(dsAsset('js/lib/tel-input/js/intlTelInput.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/lib/moment.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/lib/jquery.steps/jquery.steps.min.js')); ?>"></script>
	<link href="<?php echo e(dsAsset('js/lib/jquery.steps/jquery.steps.css')); ?>" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo e(dsAsset('site/css/website.css')); ?>">
	<script src="<?php echo e(dsAsset('site/js/custom/website.js')); ?>"></script>
	<style>
		:root {
		--theamColor: <?php echo e($appearance["theam_color"]); ?>;
		--theamHoverColor: <?php echo e($appearance["theam_hover_color"]); ?>;
		--theamActiveColor: <?php echo e($appearance["theam_active_color"]); ?>;
		--theamMenuColor: <?php echo e($appearance["menu_color"]); ?>;
		--theamMenuColor2: <?php echo e($appearance["menu_color2"]); ?>;
		--theamColorRgba:<?php echo e(hex2Rgba($appearance["theam_color"],0.1)); ?>;
	}
	</style>

	<?php echo $__env->yieldPushContent('css'); ?>
</head>

<body id="process_notifi">

	<div class="container-fluid row g-0 px-5 bg-primary" style="min-height: 30px;">
      
	  <div class="col-12 text-center text-md-start col-md-6 pt-3"> 
		  <p class="text-light">BIENVENIDOS A NATURALY SPA</p>
	  </div>
	  <div class="col-12 pb-2 col-md-6 pt-md-2">		  
		  <ul class="nav justify-content-center justify-content-md-end">
			  
			  <li class="nav-item">
				  <a target="_blank" class="nav-link" href="<?php echo e($appearance->facebook_link); ?>">
					  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="text-light bi bi-facebook" viewBox="0 0 16 16">
						  <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
						</svg> 
				  </a>
			  </li>
			  <li class="nav-item">
				  <a target="_blank" class="nav-link" href="<?php echo e($appearance->instagram_link); ?>">
					  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="text-light bi bi-instagram" viewBox="0 0 16 16">
						  <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
						</svg>
				  </a>
			  </li>
			  <li class="nav-item">
				  <a target="_blank" class="nav-link" href="<?php echo e($appearance->youtube_link); ?>">
					  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="text-light bi bi-youtube" viewBox="0 0 16 16">
						  <path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/>
						</svg>
				  </a>
			  </li>
			  <li class="nav-item">
				  <a target="_blank" class="nav-link" href="<?php echo e($appearance->twitter_link); ?>">
					  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="text-light bi bi-whatsapp" viewBox="0 0 16 16">
						  <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/>
						</svg>
				  </a>
			  </li>
			 
		  </ul>
	  </div>
  	</div>

	<div class="container-fluid">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
              <a class="navbar-brand d-sm-none d-sm-block" href="<?php echo e(route('site.home')); ?>"> 
                <img src="<?php echo e(dsAsset($appearance->logo)); ?>" alt="Logo" width="100%" height="100%">
              </a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <button class="menu-letra mt-1 mx-1 btn btn-outline-primary text-dark btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-badge pb-1" viewBox="0 0 16 16">
                            <path d="M6.5 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1h-3zM11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"></path>
                            <path d="M4.5 0A2.5 2.5 0 0 0 2 2.5V14a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2.5A2.5 2.5 0 0 0 11.5 0h-7zM3 2.5A1.5 1.5 0 0 1 4.5 1h7A1.5 1.5 0 0 1 13 2.5v10.795a4.2 4.2 0 0 0-.776-.492C11.392 12.387 10.063 12 8 12s-3.392.387-4.224.803a4.2 4.2 0 0 0-.776.492V2.5z"></path>
                            </svg>
                        <a href="<?php echo e(route('site.menu.services')); ?>" class="text-dark">Servicios</a>
                    </button>

                    <button class="menu-letra mt-1 mx-1 py-1 btn btn-outline-primary text-dark btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gift pb-1" viewBox="0 0 16 16">
                            <path d="M3 2.5a2.5 2.5 0 0 1 5 0 2.5 2.5 0 0 1 5 0v.006c0 .07 0 .27-.038.494H15a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a1.5 1.5 0 0 1-1.5 1.5h-11A1.5 1.5 0 0 1 1 14.5V7a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h2.038A2.968 2.968 0 0 1 3 2.506V2.5zm1.068.5H7v-.5a1.5 1.5 0 1 0-3 0c0 .085.002.274.045.43a.522.522 0 0 0 .023.07zM9 3h2.932a.56.56 0 0 0 .023-.07c.043-.156.045-.345.045-.43a1.5 1.5 0 0 0-3 0V3zM1 4v2h6V4H1zm8 0v2h6V4H9zm5 3H9v8h4.5a.5.5 0 0 0 .5-.5V7zm-7 8V7H2v7.5a.5.5 0 0 0 .5.5H7z"/>
                          </svg>
                        <a href="<?php echo e(route('site.home')); ?>" class="text-dark">Promos</a>
                    </button>

                    <button class="menu-letra mt-1 mx-1 py-2 btn btn-outline-primary text-dark btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-rolodex pb-1" viewBox="0 0 16 16">
                            <path d="M8 9.05a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                            <path d="M1 1a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h.5a.5.5 0 0 0 .5-.5.5.5 0 0 1 1 0 .5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5.5.5 0 0 1 1 0 .5.5 0 0 0 .5.5h.5a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H6.707L6 1.293A1 1 0 0 0 5.293 1H1Zm0 1h4.293L6 2.707A1 1 0 0 0 6.707 3H15v10h-.085a1.5 1.5 0 0 0-2.4-.63C11.885 11.223 10.554 10 8 10c-2.555 0-3.886 1.224-4.514 2.37a1.5 1.5 0 0 0-2.4.63H1V2Z"/>
                        </svg>
                        <a href="<?php echo e(route('site.contact')); ?>" class="text-dark">Contacto</a>
                    </button>

                    

                </ul> 
                <a class="navbar-brand d-none d-sm-none d-md-none d-lg-block" href="<?php echo e(route('site.home')); ?>"> 
                    <img src="<?php echo e(dsAsset('img/logo-2-naturaly.png')); ?>" alt="Bootstrap" width="100%" height="100%">
                </a>
    
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    
                    <?php if(auth()->check() && auth()->user()->user_type==2): ?> 
                    <button class="menu-letra mt-1 mx-1 py-2 btn btn-primary text-light btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-right pb-1" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"></path>
                            <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"></path>
                            </svg>
                        <a href="<?php echo e(route('login')); ?>" class="text-light"> Mi Panel</a>
                    </button>
                    <?php else: ?>
                    <button class="menu-letra mt-1 mx-1 py-2 btn btn-primary text-dark btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-in-right pb-1" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"></path>
                            <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"></path>
                            </svg>
                        <a href="<?php echo e(route('login')); ?>" class="text-dark"> <?php echo e(translate('Sign In')); ?></a>
                    </button>

                    <button class="menu-letra mt-1 mx-1 py-2 btn btn-dark text-light btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="pb-1 bi bi-box-arrow-in-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"></path>
                            <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"></path>
                            </svg>
                        <a href="<?php echo e(route('register')); ?>" class="text-light"> <?php echo e(translate('Sign Up')); ?>   </a>
                    </button>
                    <?php endif; ?>

                    <button class="menu-letra mt-1 mx-1 py-2 btn btn-danger text-light btn-xs-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="pb-1 bi bi-box-arrow-in-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"></path>
                            <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"></path>
                            </svg>
                         <a href="<?php echo e(route('site.appoinment.booking')); ?>" class="text-light"> <?php echo e(translate('Book Now')); ?></a>
                    </button> 
                </ul>
    
              </div>
            </div>
        </nav>
    </div>   

	 
	<!--end header -->

	<?php echo $__env->yieldContent('content'); ?>;

	<!-- Start footer-->

    <div class="container-fluid bg-primary py-5">
        <div class="container py-5">
            <div class="row">
                 
                <div class="col-12 col-md-6 pt-4 centrar">
                    <img class="img-fluid" src="<?php echo e(dsAsset('img/logo-2-naturaly-noche.png')); ?>" alt="Logo" width="297" height="54">
                    <p>En Naturaly sentirás el placer de recuperar tus fuerzas.</p>
                </div>
                <div class="col-12 col-md-2 pt-3">
                        <ul class="list-group">
                            <li class="list-group-item border-0 bg-primary menu-footer">
                                <h4 class="pituco th3 text-md-start text-center">Menú</h4>
                            </li>
                            <?php $__currentLoopData = $menuList->where('site_menu_id', 0)->skip(0)->take(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mTop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li class="d-grid gap-2 list-group-item border-0 bg-primary p-0">
                                <a class="btn btn-block text-md-start text-dark" href="<?php echo e(route($mTop->route)); ?>" style="font-size: 1.4rem !important;"><?php echo e($mTop->name); ?>  </a>
                            </li>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                             
                        </ul>
                </div>

                <div class="col-12 col-md-2 pt-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 bg-primary menu-footer">
                            <h4 class="pituco th3 text-md-start text-center">Menú</h4>
                        </li>
                        <?php $__currentLoopData = $menuList->where('site_menu_id', 0)->skip(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mTop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li class="d-grid gap-2 list-group-item border-0 bg-primary p-0">
                            <a class="btn btn-block text-md-start text-dark" href="<?php echo e(route($mTop->route)); ?>" style="font-size: 1.4rem !important;"><?php echo e($mTop->name); ?> </a>
                        </li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                         
                    </ul>
                </div>

                <div class="col-12 col-md-2 pt-3">
                    <ul class="list-group">
                    <li class="list-group-item border-0 bg-primary menu-footer">
                        <h4 class="pituco th3 text-md-start text-center">Panel</h4>
                    </li>
                    <?php if(auth()->check() && auth()->user()->user_type==2): ?> 
                    <li class="d-grid gap-2 list-group-item border-0 bg-primary p-0"> 
                       <a class="btn btn-block text-md-start text-dark" href="<?php echo e(route('login')); ?>" style="font-size: 1.4rem !important;">  Mi Panel</a>
                    </li>  
                        
                     
                    <?php else: ?>

                    <li class="d-grid gap-2 list-group-item border-0 bg-primary p-0"> 
                       <a class="btn btn-block text-md-start text-dark" href="<?php echo e(route('login')); ?>" style="font-size: 1.4rem !important;"> <?php echo e(translate('Sign In')); ?></a>
                    </li> 

                    <li class="d-grid gap-2 list-group-item border-0 bg-primary p-0"> 
                       <a class="btn btn-block text-md-start text-dark" href="<?php echo e(route('register')); ?>" style="font-size: 1.4rem !important;"> <?php echo e(translate('Sign Up')); ?></a>
                    </li>  
                     
                    <?php endif; ?> 
                     
                     
                    </ul>
                </div>  

            </div>  
        </div>

        <div class="col-12" style="border:1px solid #e8d7d7;">

        </div>

        <div class="container px-5">
            <div class="row g-0 py-3 bg-primary text-white" style="min-height: 30px;">
              
              <div class="col-5 pt-3"> 
                <p class="text-dark" style="font-size: 12px!important;">
                Copyright &copy; <?php echo e(now()->year); ?> Derechos Reservados | <?php echo e($appearance->app_name); ?>

                </p>
              </div>
       

              <div class="col-7"><div class="ms-auto"></div>
                  <ul class="nav justify-content-end">
                      
                      <li class="nav-item">
                          <a class="nav-link" target="_blank" href="<?php echo e($appearance->facebook_link); ?>">
                              <button type="button" class="btn btn-outline-dark">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                                  <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"></path>
                                  </svg>
                              </button>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" target="_blank" href="<?php echo e($appearance->instagram_link); ?>">
                              <button type="button" class="btn btn-outline-dark">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-instagram" viewBox="0 0 16 16">
                                  <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"></path>
                              </svg>
                              </button>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" target="_blank" href="<?php echo e($appearance->youtube_link); ?>">
                              <button type="button" class="btn btn-outline-dark">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-youtube" viewBox="0 0 16 16">
                                      <path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"></path>
                                  </svg>
                              </button>
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" target="_blank" href="<?php echo e($appearance->twitter_link); ?>">
                              <button type="button" class="btn btn-outline-dark">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                      <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"></path>
                                  </svg>
                              </button>
                          </a>
                      </li>                   
                  </ul>
              </div>
            </div> 
    
        </div>
    </div> 

 
	
	<!-- End footer -->
	<script src="<?php echo e(dsAsset('site/assets/js/bootstrap.min.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('site/assets/js/popper.min.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('site/assets/js/easing.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('site/assets/js/lib/owl-carousel/owl.carousel.min.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('site/assets/js/lib/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('site/assets/js/main.js')); ?>"></script>
	<!--notify JS-->
	<script src="<?php echo e(dsAsset('js/lib/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')); ?>"></script>
	<!--JQ bootstrap validation-->
	<script src="<?php echo e(dsAsset('js/lib/assets/js/plugin/jquery-bootstrap-validation/jqBootstrapValidation.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/lib/xd-dpicker/build/jquery.datetimepicker.full.min.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/site.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/lib/js-manager.js')); ?>"></script>
	<script src="<?php echo e(dsAsset('js/lib/js-message.js')); ?>"></script>
	<?php echo $__env->yieldPushContent('scripts'); ?>
	
</body>

</html><?php /**PATH D:\Laravel\naturaly-3\resources\views/site/layouts/site.blade.php ENDPATH**/ ?>