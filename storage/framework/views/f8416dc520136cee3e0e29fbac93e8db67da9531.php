<?php $__env->startSection('content'); ?>
<!-- Start Slide-->
	<!-- carrusel -->
	<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>

        <div class="carousel-inner">
          <div class="carousel-item">
            <img src="img/portada.jpg" class="d-block w-100" alt="slider sitio Web">
            <div class="carousel-caption opacity-50 d-none d-md-block">
              <div class="row my-5 bg-primary pb-2"> 
                <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Descubre el poder de tu belleza</h1>
              </div>
            </div>
          </div>

          <div class="carousel-item active">
            <img src="img/portada.jpg" class="d-block w-100" alt="slider sitio Web">
            <div class="carousel-caption opacity-50 d-none d-md-block">
                <div class="row my-5 bg-primary pb-2"> 
                    <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Descubre el poder de tu belleza</h1>
                  </div>
            </div>
          </div>
          
          <div class="carousel-item active">
            <img src="img/portada.jpg" class="d-block w-100" alt="slider sitio Web">
            <div class="carousel-caption d-none d-md-block">
                <div class="row my-5 bg-primary pb-2"> 
                    <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Descubre el poder de tu belleza</h1>
                  </div>
            </div>
          </div>

        </div>
        
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Anterior</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Siguiente</span>
        </button>
    </div>

	<!-- Texto -->
	<div class="container py-5 pt-5">
        <div class="row">
            <div class="col-12">
                <img src="img/i-naturaly-spa.png" class="rounded mx-auto d-block img-fluid" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">BENEFICIOS</h2>
                <p class="text-center p-5">En Naturaly SPA siempre encontrarás un ambiente acogedor, cómodo, natural y cálido; donde podrás relajarse completamente, alejarse de la rutina o del trabajo estresante. Brindamos servicios variados que renovarán su mente y cuerpo hasta relajarse por completo.
                </p>
            </div>

            <div class="row py-md-5 py-1 px-5">
                    
                <div class="col-6 col-md-4 py-3">
                    <img src="img/limpieza-facial-spa-sullana.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                    <h3 class="text-center pituco text-dark pt-5 th3">CONFIABLE Y <br> ELEGANTE</h3>
                </div>
                <div class="col-6 col-md-4 py-3">
                    <img src="img/masajes-spa-sullana.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                    <h3 class="text-center pituco text-dark pt-5 th3">AMBIENTE <br> ACOGEDOR</h3>
                </div>
                <div class="col-12 col-md-4 py-3">
                    <img src="img/ambiente-acogedor-naturaly-spa-sullana.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                    <h3 class="text-center pituco text-dark pt-5 th3">CALIDEZ DEL <br> SERVICIO</h3>
                </div>
                
            </div>


        </div>
    </div>
	<!-- servicios -->	
	<div class="container-fluid fondo-web py-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                <img src="img/i-naturaly-spa.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">SERVICIOS</h2>
                <p class="text-center pb-5 pt-3">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>   
                
                <div class="row pb-5">
                    <div class="col-12 col-md-2">                        
                    </div>
                    <div class="col-12 col-md-4 pt-5">
                        <img class="img-fluid" src="img/masajes-descontracturantes-sullana.png" alt="Naturaly SPA - Sullana">
                    </div>

                    <div class="col-12 col-md-4 pt-5">                        
                         <h4 class="proxima px-1 px-md-5 pt-5">DESTACADO</h4>
                         <h3 class="pituco thdestacado text-justify px-1 px-md-5 py-4">Masajes descontracturantes, relajantes y terapéuticos.</h3>
                         
                         <p class="text-justify px-1 px-md-5">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                         </p>  
                         <p class="px-1 px-md-5 pt-3"><button type="button" class="btn btn-primary text-dark">VER MÁS</button></p>
                    </div>

                    <div class="col-12 col-md-2">                        
                    </div>

                </div>

                <div class="row py-5">
                    <div class="col-12 col-md-2"> 
                    </div> 
                    <div class="col-6 col-md-2"> 
                        <img class="img-fluid" src="img/servicios-1-terapia-sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                       
                        <h4 class="proxima text-dark text-center pt-3 text-uppercase">Terapias </h4>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>                      
                    <div class="col-6 col-md-2">  
                        <img class="img-fluid" src="img/servicios-2-depilacion--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                      
                        <h4 class="proxima text-dark text-center pt-3 text-uppercase">Depilación</h4>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-6 col-md-2">            
                        <img class="img-fluid" src="img/servicios-3-faciales--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">            
                        <h4 class="proxima text-dark text-center pt-3 text-uppercase">Faciales</h4>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>                      
                    <div class="col-6 col-md-2">          
                        <img class="img-fluid" src="img/servicios-4-reductor--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
                        <h4 class="proxima text-dark text-center pt-3 text-uppercase">Reductores</h4>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-12 col-md-2"> 
                    </div>

                </div>  

            </div>
        </div>
    </div> 

	<!-- fin servicios -->
    <div class="container p-5">
        <div class="row p-5 border border-dark rounded-pill">
            <div class="col-12 div col-md-6">
                <h4 class="proxima px-5 pt-5 text-uppercase">En qué consiste</h4>
                <h4 class="pituco th3 text-justify px-5 pb-3">Tri-Pack: 3 tratamientos en 1</h4>
                
                <p class="text-justify px-5">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>  
                <p class="px-5 pt-3"> 
                    <button type="button" class="btn btn-primary text-dark">VER MÁS</button>
                </p>
            </div>

            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-1.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-2.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-3.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>

        </div>
    </div>
     
    <div class="container-fluid fondo-video">
        <div class="row">
            <div class="col-12 text-center text-light" style="padding: 250px 0;">
                <svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" fill="currentColor" class="bi bi-play" viewBox="0 0 16 16">
                    <path d="M10.804 8 5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z"/>
                </svg>

                <h2 class="pituco th2 pt-5 text-light">MASAJE RELAJANTE</h2>
                <p class="px-2 text-light">Libérate de las malas vibras, del estrés producto del trabajo, preocupaciones o la rutina. Prueba nuestros masajes relajantes y descontracturantes. </p>
                 
                
            </div>
        </div>
    </div>

    <div class="container p-5">
        <div class="row text-center pt-3 border border-dark pb-3">
            <div class="col-6 col-md-3">
                <span class="pituco th3">100% relajante </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="proxima th3">Masaje Manual </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="pituco th3">45 - 50 min </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="proxima th3">Recupera fuerzas </span>
            </div>

            <div class="col-6 col-md-3">
                <span class="pituco th3">Descontracturante </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="proxima th3">Pistola masajeadora </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="pituco th3">Masaje con pindas </span>
            </div>
            <div class="col-6 col-md-3">
                <span class="proxima th3">Uso de electrodos </span>
            </div>

        </div>
    </div>

    <!-- tratamientos -->
    <div class="container py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                <img src="img/i-naturaly-spa.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">TRATAMIENTOS</h2>
                <p class="text-center py-4">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>                                  
                
                <!-- tratamientos 1 -->
                <div class="row py-1 pt-1"> 

					<?php $__currentLoopData = $topService; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
									<a href="<?php echo e(route('site.service.single.details')); ?>/<?php echo e($value->sch_service_id); ?>">
									<img class="img-fluid pb-5" src="<?php echo e($value->image); ?>" alt="<?php echo e($value->title); ?>"> 
									</a>      
                            <h4 class="pituco th3">
								<a href="<?php echo e(route('site.service.single.details')); ?>/<?php echo e($value->sch_service_id); ?>">
									<?php echo e($value->title); ?> 
								</a> 
							</h4> 

                            <p class="text-center pt-3"><?php echo e(Str::limit($value->remarks,50)); ?></p>

							<?php for($i=1;$i<=5;$i++): ?> 
									<?php if($value->avgRating>=$i): ?>
									<span class="fa fa-star checked"></span>
									<?php else: ?>
										<span class="fa fa-star"></span>
								<?php endif; ?>
								<?php endfor; ?>
								(<?php echo e($value->countRating); ?>) <br><br>


                                <button type="button" class="btn btn-primary text-dark text-uppercase">
								<a href="<?php echo e(route('site.service.single.details')); ?>/<?php echo e($value->sch_service_id); ?>">	ver más</a>
                                         
								</button>

								<button type="button" class="btn btn-primary text-dark text-uppercase">
								<a href="<?php echo e(route('site.appoinment.booking')); ?>">Reservar</a> 
								</button>
                             
                        </div>
                    </div> 
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
 

                </div> 
               
                <!-- tratamientos 2 -->
                <div class="row py-1 pt-1">
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/masaje-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Masaje terapéutico</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                            <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>

                    <div class="col-6 col-md-3 px-1 px-md-5 my-2"> 
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/reductor-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Reductor abdomen</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>  
                                        
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">          
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/acner-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Tramiento acné</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>

                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/limpieza-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Limpieza de Cutis</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div> 
                </div>
 

            </div>
        </div>
    </div>

    <!-- galería -->
    <div class="container-fluid fondo-web py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                
                <h2 class="text-center pituco text-primary pt-5 th2">GALERÍA</h2>
                <p class="text-center pt-3">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>                                  
                
                <div class="row py-5">
                    <div class="col-12 col-md-2"> 
                    </div> 
                    <div class="col-2 col-md-2"> 
                        <img class="img-fluid pb-5" src="img/galeria1.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">    
                        <img class="img-fluid pt-3" src="img/galeria2.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                                          
                       
                    </div>                      
                     
                    <div class="col-8 col-md-4">            
                        <img class="img-fluid" src="img/galeria-servicio-limpieza-cutis.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">            
                        
                    </div>                      
                    <div class="col-2 col-md-2">          
                        <img class="img-fluid pb-5" src="img/galeria4.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                        <img class="img-fluid pt-3" src="img/galeria5.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                                    
                         
                    </div>
                    <div class="col-12 col-md-2"> 
                    </div>

                </div> 
               

            </div>
        </div>
    </div>
    
    <!-- +2500 -->
    <div class="container py-2">
        <div class="row py-3 pb-5">
            <div class="col-12 col-md-1"></div>
            <div class="col-12 col-md-3 text-center">
                <h2 class="pituco th1">+2500</h2>
            </div>
            <div class="col-12 col-md-5">
                <h2 class="text-center text-md-start pituco th2">Pacientes atendidos con <span class="azul">éxito</span></h2>
            </div>
            <div class="col-12 col-md-1"></div>
        </div>
    </div>

    

<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.site', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/acampos/Workspace/naturaly-3/resources/views/site/index.blade.php ENDPATH**/ ?>