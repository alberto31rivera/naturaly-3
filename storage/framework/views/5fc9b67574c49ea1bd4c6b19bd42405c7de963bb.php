<?php $__env->startSection('content'); ?>
<link href="<?php echo e(dsAsset('site/css/custom/choose-payment-method.css')); ?>" rel="stylesheet" />
<div class="row">
    <div class="col-md-8 offset-md-2 mt-4">
        <div class="card">
            <div class="card-header">
                Cargar Imagen de pago
            </div>
            <div class="card-body">
                <?php if($typePayment == 5): ?>
                <img src="<?php echo e(asset('img/yape.jpg')); ?>" style="margin-left: 25%; width: 50%; margin-right: 25%" />
                <?php elseif($typePayment == 6): ?>
                <img src="<?php echo e(asset('img/plin.jpg')); ?>" style="margin-left: 25%; width: 50%; margin-right: 25%"/>
                <?php elseif($typePayment == 7): ?>
                <img src="<?php echo e(asset('img/transferencia.jpg')); ?>" style="margin-left: 25%; width: 50%; margin-right: 25%"/>
                <?php endif; ?>

                <form action="<?php echo e(route('store-image-payment')); ?>" method="post" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="bookingId" value="<?php echo e($bookingId); ?>">

                    <input type="file" name="image" class="form-control mt-3" id="">
                    <button type="submit" class="btn btn-success w-100 mt-3">Enviar Pago</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo e(dsAsset('site/js/custom/choose-payment-method.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.site', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/acampos/Workspace/naturaly-3/resources/views/paymentimages/add.blade.php ENDPATH**/ ?>