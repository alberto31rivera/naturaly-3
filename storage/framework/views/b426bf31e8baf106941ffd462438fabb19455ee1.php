	
	<?php $__env->startSection('content'); ?>
	<!--start banner section -->
	<section class="banner-area position-relative" style="background:url(<?php echo e($appearance->background_image); ?>) no-repeat;min-height: 300px;background-size: cover;">
		  
		<div class="overlay overlay-bg pt-5"></div>
		<div class="container py-5">
			<div class="row py-5">
				<div class="col-md-12 py-5">
					<div class="position-relative text-center py-5">
						<h1 class="pituco th2 text-capitalize mb-3 text-white"><?php echo e(translate('Our Services')); ?></h1>
						<a class="text-white" href="<?php echo e(route('site.home')); ?>"><?php echo e(translate('Home')); ?> </a>
						<i class="icofont-long-arrow-right text-white"></i>
						<a class="text-white" href="<?php echo e(route('site.menu.services')); ?>"> <?php echo e(translate('Service')); ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end banner section -->

	 <!-- tratamientos -->
	 <div class="container py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                <img src="img/i-naturaly-spa.png" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">NUESTROS TRATAMIENTOS</h2>
                <p class="text-center py-4">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>                                  
                
                <!-- tratamientos 1 -->
                <div class="row py-1 pt-1"> 

					<?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
									<a href="<?php echo e(route('site.service.single.details')); ?>/<?php echo e($value->sch_service_id); ?>">
									<img class="img-fluid pb-5" src="<?php echo e($value->image); ?>" alt="<?php echo e($value->title); ?>"> 
									</a>      
                            <h4 class="pituco th3">
								<a href="<?php echo e(route('site.service.single.details')); ?>/<?php echo e($value->sch_service_id); ?>">
									<?php echo e($value->title); ?> 
								</a> 
							</h4> 

                            <p class="text-center pt-3"><?php echo e(Str::limit($value->remarks,50)); ?></p>

							<?php for($i=1;$i<=5;$i++): ?> 
									<?php if($value->avgRating>=$i): ?>
									<span class="fa fa-star checked"></span>
									<?php else: ?>
										<span class="fa fa-star"></span>
									<?php endif; ?>
								<?php endfor; ?>
								(<?php echo e($value->countRating); ?>) <br><br>


                                

								<button type="button" class="btn btn-danger text-light text-uppercase">
								<a class="text-light" href="<?php echo e(route('site.appoinment.booking')); ?>"> Reservar</a> 
								</button>
                             
                        </div>
                    </div> 
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
 

                </div>  

            </div>
        </div>
    </div>

	<!-- fin servicios -->
    <div class="container p-5">
        <div class="row p-5 border border-dark rounded-pill">
            <div class="col-12 div col-md-6">
                <h4 class="proxima px-5 pt-5 text-uppercase">En qué consiste</h4>
                <h4 class="pituco th3 text-justify px-5 pb-3">Tri-Pack: 3 tratamientos en 1</h4>
                
                <p class="text-justify px-5">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>  
                <p class="px-5 pt-3"> 
                    <button type="button" class="btn btn-primary text-dark">VER MÁS</button>
                </p>
            </div>

            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-1.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-2.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/tripack-3.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>

        </div>
    </div>
	<!-- End service Area -->
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.index2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Laravel\naturaly-3\resources\views/site/services.blade.php ENDPATH**/ ?>