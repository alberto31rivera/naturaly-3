<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentImages extends Model
{
    use HasFactory;

    protected $fillable = [
        'serviceBookingId',
        'status',
        'image_payment',
    ];
}
