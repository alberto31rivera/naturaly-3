<?php

namespace App\Http\Controllers;

use App\Models\Booking\SchServiceBooking;
use App\Models\PaymentImages;
use Illuminate\Http\Request;

class PaymentImagesController extends Controller
{
    public function addImage($bookingId, $typePayment)
    {
        return view('paymentimages.add', compact('typePayment', 'bookingId'));
    }

    public function storeImage(Request $request)
    {
        $updateData = SchServiceBooking::where('id', $request->bookingId)->first();
        if($request->hasFile("image"))
        {
            $imagen = $request->file("image");
            $nombreimagen = date('Ymdhis').".".$imagen->guessExtension();
            $ruta = public_path("img/imagesPayments/");

            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);
        }

        $updateData->imagen_pago = $nombreimagen;
        $updateData->save();

        return redirect('/client-dashboard');
    }
}
