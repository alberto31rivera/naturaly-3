@extends('site.layouts.site')
@section('content')

<!--start banner section -->

	<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>

        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{dsAsset('img/single-servicios-spa.jpg')}}" class="d-block w-100" alt="...">
            <div class="carousel-caption opacity-50 d-none d-md-block">
              <div class="row my-5 bg-primary pb-2"> 
                <h1 class="pt-3 fs-1 text-uppercase pituco">Descubre el poder de tu belleza</h1>
              </div>
            </div>
          </div> 
           
        </div>
        
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Anterior</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Siguiente</span>
        </button>
    </div>


	<div class="container pt-5">
        <div class="row py-2 pt-5">
            <div class="col-12">
                <img src="{{dsAsset('img/i-naturaly-spa.png') }}" class="pt-5 rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">{{$serviceDetails->title}}</h2>
                
                <div class="row">
                   
                    <div class="col-12 px-md-5 px-0">
                        <p class="text-center p-5">{{$serviceDetails->remarks}}
                        </p>
                    </div> 
                </div>  
            </div>
        </div>
    </div> 

	<div class="container">
        <div class="row pb-5">
            <div class="col-12 col-md-2">                        
            </div>
            <div class="col-12 col-md-4 pt-5">
                <img class="img-fluid" src="{{dsAsset($serviceDetails->image)}}" alt="{{$serviceDetails->title}}">
            </div>
            <div class="col-12 col-md-4 pt-5">                        
                 
                <div class="row my-2">
                    <div class="col-12 bg-primary pt-2">
                        <h4 class="proxima text-dark text-uppercase text-center">{{$serviceDetails->title}} </h4>
                    </div>
                     
                </div> 
				 
                <div class="row my-2">
                    <div class="col-12 col-md-6 bg-dark pt-2">
                        <h4 class="proxima text-light text-uppercase">Precio: </h4>
                    </div>
                    <div class="col-12 col-md-6 fw-bold bg-primary text-center pt-2 text-dark text-uppercase">
                        S/ {{$serviceDetails->price}}
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col-12 col-md-6 bg-dark pt-2">
                        <h4 class="proxima text-light text-uppercase">Tiempo de la sesión: </h4>
                    </div>
                    <div class="col-12 col-md-6 fw-bold bg-primary text-center pt-2 text-dark text-uppercase">
					{{$serviceDetails->time_slot_in_time}} min
                    </div>
                    <p class="pt-3">
                        Lorem ipsum dolor sit amet, 
                        consectetur adipisicing elit. Porro debitis, 
                        eveniet nostrum libero repudiandae possimus, 
                        animi tempore qui deserunt molestias hic a facere
                        rem quibusdam ipsum consectetur iusto optio error.</p>
                </div>

                <div class="row my-2">
                    <div class="col-12 pt-2 text-center">
                        <button type="button" class="btn btn-danger text-uppercase"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                            <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
                            <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                          </svg><a href="{{route('site.appoinment.booking')}}" class="text-light"> reservar</a>   </button>
                    </div>
                     
                </div> 

            </div>

            <div class="col-12 col-md-2">  

            </div>

        </div>
    </div>

    <div class="container border border-dark">
        <div class="row py-5">
            <div class="col-12 div col-md-6 p-5">
                <h4 class="proxima px-5 pt-5 text-uppercase">En qué consiste</h4>
                <h4 class="pituco th3 text-justify px-5 pb-3">{{$serviceDetails->title}}</h4>
                
                <p class="text-justify px-5">Falta agregar detalles! Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. reductores, depilaciones; entre otros, brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. reductores, depilaciones siguiendo altos estándares de calidad. 
                </p>  
                <div class="d-grid gap-2 px-5">
                    <button class="btn btn-primary text-uppercase" type="button">
					
					<h4><a href="{{route('site.appoinment.booking')}}"> Reservar</a>
					</h4>
				
					</button>
                   
                </div>
            </div>

            <div class="col-4 div col-md-3 mx-auto d-block">
                <img class="img-fluid" src="{{dsAsset('img/i-naturaly-spa.png') }}" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>

            <div class="col-4 div col-md-3 mx-auto d-block">
                <img class="img-fluid" src="{{dsAsset('img/i-naturaly-spa.png') }}" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
             

        </div>
    </div>    
      
    <div class="container-fluid fondo-web">
        <div class="row pt-5">
            <div class="col-12"> 
               

                <div class="row py-5">
                    <div class="col-12 col-md-2"> 
                    </div> 
                    <div class="col-6 col-md-2"> 
                        <img class="img-fluid" src="img/servicios-1-terapia-sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                       
                        
                        <p>L1222orem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>                      
                    <div class="col-6 col-md-2">  
                        <img class="img-fluid" src="img/servicios-2-depilacion--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                      
                         
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-6 col-md-2">            
                        <img class="img-fluid" src="img/servicios-3-faciales--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">            
                         
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>                      
                    <div class="col-6 col-md-2">          
                        <img class="img-fluid" src="img/servicios-4-reductor--sullana.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
                       
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
                    </div>
                    <div class="col-12 col-md-2"> 
                    </div>

                </div> 

            </div>
        </div>
    </div>


	<!-- tratamientos -->
    <div class="container py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                <img src="{{dsAsset('img/i-naturaly-spa.png') }}" class="rounded mx-auto d-block" alt="Naturaly SPA - Sullana">
                <h2 class="text-center pituco text-primary pt-5 th2">TRATAMIENTOS</h2>
                <p class="text-center py-4">Estamos capacitados para brindarle servicios de masajes y tratamientos corporales y faciales, reductores, depilaciones; entre otros, siguiendo altos estándares de calidad. 
                </p>                                  
                
                <!-- tratamientos 1 -->
                <div class="row py-1 pt-1"> 

					@foreach ($topService as $value)
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
									<a href="{{route('site.service.single.details')}}/{{$value->sch_service_id}}">
									<img class="img-fluid pb-5" src="{{$value->image}}" alt="{{$value->title}}"> 
									</a>      
                            <h4 class="pituco th3">
								<a href="{{route('site.service.single.details')}}/{{$value->sch_service_id}}">
									{{$value->title}} 
								</a> 
							</h4> 

                            <p class="text-center pt-3"> 
                            {{ Str::limit($value->remarks,50) }}
                            </p>

							@for($i=1;$i<=5;$i++) 
									@if ($value->avgRating>=$i)
									<span class="fa fa-star checked"></span>
									@else
										<span class="fa fa-star"></span>
								@endif
								@endfor
								({{$value->countRating}}) <br><br>


                                <button type="button" class="btn btn-primary text-dark text-uppercase">
								<a href="{{route('site.service.single.details')}}/{{$value->sch_service_id}}">	ver más</a>
								</button>

								<button type="button" class="btn btn-primary text-dark text-uppercase">
								<a href="{{route('site.appoinment.booking')}}">Reservar</a> 
								</button>
                             
                        </div>
                    </div> 
					@endforeach 
 

                </div> 
               
                <!-- tratamientos 2 -->
                <div class="row py-1 pt-1">
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/masaje-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Masaje terapéutico</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                            <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>

                    <div class="col-6 col-md-3 px-1 px-md-5 my-2"> 
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/reductor-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Reductor abdomen</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>  
                                        
                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">          
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/acner-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Tramiento acné</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div>

                    <div class="col-6 col-md-3 px-1 px-md-5 my-2">
                        <div class="text-center pb-5 px-1 cuadron">
                            <img class="img-fluid pb-5" src="img/limpieza-tratamiento.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                             
                            <h4 class="pituco th3">Limpieza de Cutis</h4>
                            <p class="text-center pt-3">Estamos  amientos corporales y faciales, siguiendo altos estándares de calidad. </p>
                                   <button type="button" class="btn btn-primary text-dark text-uppercase">ver más</button>
                             
                        </div>
                    </div> 
                </div>
 

            </div>
        </div>
    </div>						


 



<!-- End popular service -->
@endsection