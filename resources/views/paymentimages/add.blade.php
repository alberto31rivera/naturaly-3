@extends('site.layouts.site')
@section('content')
<link href="{{ dsAsset('site/css/custom/choose-payment-method.css') }}" rel="stylesheet" />
<div class="row">
    <div class="col-md-8 offset-md-2 mt-4">
        <div class="card">
            <div class="card-header">
                Cargar Imagen de pago
            </div>
            <div class="card-body">
                @if($typePayment == 5)
                <img src="{{ asset('img/yape.jpg') }}" style="margin-left: 25%; width: 50%; margin-right: 25%" />
                @elseif($typePayment == 6)
                <img src="{{ asset('img/plin.jpg') }}" style="margin-left: 25%; width: 50%; margin-right: 25%"/>
                @elseif($typePayment == 7)
                <img src="{{ asset('img/transferencia.jpg') }}" style="margin-left: 25%; width: 50%; margin-right: 25%"/>
                @endif

                <form action="{{ route('store-image-payment') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="bookingId" value="{{ $bookingId }}">

                    <input type="file" name="image" class="form-control mt-3" id="">
                    <button type="submit" class="btn btn-success w-100 mt-3">Enviar Pago</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ dsAsset('site/js/custom/choose-payment-method.js') }}"></script>
@endsection